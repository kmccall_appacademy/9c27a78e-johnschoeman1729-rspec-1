def translate(words)
  words.split.map { |word| translate_word(word)}.join(" ")
end

def translate_word(word)
  first_vowel_index = word =~ /[aeiou]/
  first_qu = (word =~ /qu/)

  # Check if first vowel is actually "qu", if so add 1 to index.
  # This won't work if qu is followed by another consonant or "qu",
  # but I don't think there are any english words like that.
  first_vowel_index += 1 if first_qu && first_qu + 1 == first_vowel_index

  "#{word[first_vowel_index..-1]}#{word[0...first_vowel_index]}ay"
end
