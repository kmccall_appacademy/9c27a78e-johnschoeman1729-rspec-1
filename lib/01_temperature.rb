def ftoc(deg)
  (5/9.to_f)*(deg.to_f - 32)
end

def ctof(deg)
  (9/5.to_f)*deg.to_f + 32
end
