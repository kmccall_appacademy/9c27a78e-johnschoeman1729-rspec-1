def add(a,b)
  a+b
end

def subtract(a,b)
  a-b
end

def sum(arr)
  arr.inject(0,:+)
end

def multiply(*a)
  # not sure why a.inject(0,:*) doesn't work here.
  res = 1
  a.each {|x| res *= x}
  res
end

def power(a,b)
  a**b
end

def factorial(a)
  return 0 if a == 0
  fact = -> (x) { x == 1 ? 1 : x*fact[x-1] }
  fact[a]
end
