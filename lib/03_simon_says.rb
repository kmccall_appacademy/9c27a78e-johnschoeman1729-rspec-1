def echo(input)
  input
end

def shout(input)
  input.upcase
end

def repeat(input, times = 2)
  (input + " ") * (times - 1) + input
end

def start_of_word(word, num)
  word[0...num]
end

def first_word(words)
  words.split[0]
end

def titleize(words)
  little_words = ["the", "over", "and"]
  words = words.split
  arr = []
  arr << words.shift.capitalize
  words.each do |word|
     little_words.include?(word) ? arr << word : arr << word.capitalize
  end
  arr.join(" ")
end
